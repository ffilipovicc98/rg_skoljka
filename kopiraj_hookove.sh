#!/bin/bash

rm .git/hooks/pre-commit* -f

cp ./git_hooks/pre-commit.py .git/hooks/
cp ./git_hooks/pre-commit .git/hooks/

chmod 775 .git/hooks/pre-commit.py
chmod 775 .git/hooks/pre-commit
