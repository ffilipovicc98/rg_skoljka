#ifndef PROJECT_BASE_SRC_MYTEXTURE2D_HPP
#define PROJECT_BASE_SRC_MYTEXTURE2D_HPP
#include <glad/glad.h>
#include <stb_image.h>

#include <learnopengl/shader.h>
#include <rg/Error.h>

class MyTexture2D {
public:
  MyTexture2D(std::string filename, std::string uniform_shader_variable_name,
              int uniform_shader_variable_value, Shader &shader,
              GLint texture_wrap_s, GLint texture_wrap_t,
              GLint texture_min_filter, GLint texture_mag_filter)
      : m_filename(filename),
        m_uniform_shader_variable_name(uniform_shader_variable_name),
        m_uniform_shader_variable_value(uniform_shader_variable_value),
        m_shader_texture(shader), m_texture_wrap_s(texture_wrap_s),
        m_texture_wrap_t(texture_wrap_t),
        m_texture_min_filter(texture_min_filter),
        m_texture_mag_filter(texture_mag_filter) {
    glGenTextures(1, &m_id);
    glBindTexture(GL_TEXTURE_2D, m_id);

    wrap();
    filter();
    load_img();
  }

  void wrap() {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, m_texture_wrap_s);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, m_texture_wrap_t);
  }

  void filter() {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_texture_min_filter);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_texture_mag_filter);
  }

  void load_img() {
    int width, height, nChannel;
    unsigned char *data =
        stbi_load(m_filename.c_str(), &width, &height, &nChannel, 0);
    if (data) {
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
                   GL_UNSIGNED_BYTE, data);
      glGenerateMipmap(GL_TEXTURE_2D);
    } else {
      ASSERT(false, "Failed to load texture!\n");
    }
    stbi_image_free(data);

    m_shader_texture.use();
    m_shader_texture.setUniform1i(m_uniform_shader_variable_name,
                                  m_uniform_shader_variable_value);
  }

  void activate_and_bind() {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_id);
  }

  void unbound() {
    //		glBindTexture(GL_TEXTURE_2D, 0);
  }

private:
  std::string m_filename;
  std::string m_uniform_shader_variable_name;
  int m_uniform_shader_variable_value;
  Shader &m_shader_texture;
  GLint m_texture_wrap_s;
  GLint m_texture_wrap_t;
  GLint m_texture_min_filter;
  GLint m_texture_mag_filter;
  unsigned int m_id;
};

#endif // PROJECT_BASE_SRC_MYTEXTURE2D_HPP
