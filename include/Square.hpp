#ifndef PROJECT_BASE_SRC_SQUARE_HPP
#define PROJECT_BASE_SRC_SQUARE_HPP

#include "My3DObject.hpp"
#include "MyTexture2D.hpp"

class Square : public My3DObject, MyTexture2D {
public:
  Square(const char *vertexPath, const char *fragmentPath,
         std::string textureFilename);
  static void setupStaticMembers();
  void draw() override;

private:
  inline static unsigned int m_VAO;
  inline static unsigned int m_VBO;
  inline static unsigned int m_EBO;
  constexpr inline static const float m_vertices[32] = {
      // positions          // normals           // texture coords
      15.0f,  15.0f,  0.0f, 0.0f, 1.0f, 0.0f, 15.0f, 15.0f, // top right
      15.0f,  -15.0f, 0.0f, 0.0f, 1.0f, 0.0f, 15.0f, 0.0f,  // bottom right
      -15.0f, -15.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,  0.0f,  // bottom left
      -15.0f, 15.0f,  0.0f, 0.0f, 1.0f, 0.0f, 0.0f,  15.0f  // top left
  };
  constexpr inline static const unsigned int m_indices[6] = {
      0, 1, 3, // first triangle
      1, 2, 3  // second triangle
  };
};

#endif // PROJECT_BASE_SRC_SQUARE_HPP
