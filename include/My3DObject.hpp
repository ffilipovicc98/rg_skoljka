#ifndef PROJECT_BASE_SRC_MY3DOBJECT_HPP
#define PROJECT_BASE_SRC_MY3DOBJECT_HPP

#include <learnopengl/shader.h>

class My3DObject {
public:
  My3DObject(const char *vertexPath, const char *fragmentPath);
  virtual ~My3DObject();
  void setModelMatrix(const glm::mat4 &model);
  void setViewMatrix(const glm::mat4 &view);
  void setProjectionMatrix(const glm::mat4 &projection);
  void setMVPMatrices(const glm::mat4 &model, const glm::mat4 &view,
                      const glm::mat4 &projection);
  virtual void draw();
  Shader &getShader();

private:
  glm::mat4 m_model;
  glm::mat4 m_view;
  glm::mat4 m_projection;

protected:
  Shader *m_shader;
};

#endif // PROJECT_BASE_SRC_MY3DOBJECT_HPP
