#include "Cube.hpp"

Cube::Cube(const char *vertexPath, const char *fragmentPath)
    : My3DObject(vertexPath, fragmentPath) {}

void Cube::setupStaticMembers() {
  glGenVertexArrays(1, &m_VAO);

  glGenBuffers(1, &m_VBO);

  glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glBindVertexArray(m_VAO);

  // position attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
  glEnableVertexAttribArray(0);
}

void Cube::draw() {
  My3DObject::draw();

  glBindVertexArray(m_VAO);
  glDrawArrays(GL_TRIANGLES, 0, 36);
  glBindVertexArray(0);
}