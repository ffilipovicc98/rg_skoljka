#include "My3DObject.hpp"

My3DObject::My3DObject(const char *vertexPath, const char *fragmentPath) {
  m_shader = new Shader(vertexPath, fragmentPath);
  m_model = glm::mat4(1.0f);
  m_view = glm::mat4(1.0f);
  m_projection = glm::mat4(1.0f);
}

My3DObject::~My3DObject() { delete m_shader; }

void My3DObject::setModelMatrix(const glm::mat4 &model) { m_model = model; }

void My3DObject::setViewMatrix(const glm::mat4 &view) { m_view = view; }

void My3DObject::setProjectionMatrix(const glm::mat4 &projection) {
  m_projection = projection;
}

void My3DObject::setMVPMatrices(const glm::mat4 &model, const glm::mat4 &view,
                                const glm::mat4 &projection) {
  setModelMatrix(model);
  setViewMatrix(view);
  setProjectionMatrix(projection);
}

void My3DObject::draw() {
  m_shader->use();

  m_shader->setMat4("model", m_model);
  m_shader->setMat4("view", m_view);
  m_shader->setMat4("projection", m_projection);
}

Shader &My3DObject::getShader() { return *m_shader; }