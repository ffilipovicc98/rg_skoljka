#include "Cube.hpp"
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include <GLFW/glfw3.h>
#include <glad/glad.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <learnopengl/camera.h>
#include <learnopengl/filesystem.h>
#include <learnopengl/model.h>
#include <learnopengl/shader.h>

#include <Square.hpp>
#include <iostream>

void framebuffer_size_callback(GLFWwindow *window, int width, int height);

void mouse_callback(GLFWwindow *window, double xpos, double ypos);

void scroll_callback(GLFWwindow *window, double xoffset, double yoffset);

void processInput(GLFWwindow *window);

void key_callback(GLFWwindow *window, int key, int scancode, int action,
                  int mods);

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// camera

float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

// timing
float deltaTime = 0.0f;
float lastFrame = 0.0f;

class DirLight {
public:
  DirLight() {
    direction = glm::vec3(-2.0f, 0.5f, 1.0);
    ambient = glm::vec3(0.1, 0.1, 0.1);
    diffuse = glm::vec3(0.8, 0.8, 0.8);
    specular = glm::vec3(1.0, 1.0, 1.0);
  }

  glm::vec3 direction;
  glm::vec3 ambient;
  glm::vec3 diffuse;
  glm::vec3 specular;
};

class PointLight {
public:
  PointLight() {
    position = glm::vec3(0.5f, 1.0, 2.0);
    ambient = glm::vec3(0.1, 0.1, 0.1);
    diffuse = glm::vec3(0.6, 0.6, 0.6);
    specular = glm::vec3(1.0, 1.0, 1.0);

    constant = 1.0f;
    linear = 0.09f;
    quadratic = 0.032f;
  }

  glm::vec3 position;
  glm::vec3 ambient;
  glm::vec3 diffuse;
  glm::vec3 specular;
  float constant;
  float linear;
  float quadratic;
};

class LightManager {
public:
  void applyLightsToShader(Shader &shader, const glm::vec3 &cameraPosition) {
    shader.use();
    shader.setVec3("pointLight.position", pointLight.position);
    //        std::cout << programState->camera.Position.x << ", "
    //        <<programState->camera.Position.y << ", "  <<
    //        programState->camera.Position.z  << std::endl;
    //		std::cout <<programState->camera.Up.x << ", "
    //<<programState->camera.Up.y << ", "  << programState->camera.Up.z  <<
    // std::endl; 		std::cout << programState->camera.Yaw << ", " <<
    // programState->camera.Pitch << std::endl;
    shader.setVec3("pointLight.ambient", pointLight.ambient);
    shader.setVec3("pointLight.diffuse", pointLight.diffuse);
    shader.setVec3("pointLight.specular", pointLight.specular);
    shader.setFloat("pointLight.constant", pointLight.constant);
    shader.setFloat("pointLight.linear", pointLight.linear);
    shader.setFloat("pointLight.quadratic", pointLight.quadratic);
    shader.setVec3("viewPos", cameraPosition);
    shader.setFloat("material.shininess", 32.0f);

    shader.setVec3("dirLight.direction", dirLight.direction);
    shader.setVec3("dirLight.ambient", dirLight.ambient);
    shader.setVec3("dirLight.diffuse", dirLight.diffuse);
    shader.setVec3("dirLight.specular", dirLight.specular);
  }

  PointLight pointLight;
  DirLight dirLight;
};

struct ProgramState {
  glm::vec3 clearColor = glm::vec3(0.1f, 0.1f, 0.1f);
  bool ImGuiEnabled = false;
  Camera camera;
  bool CameraMouseMovementUpdateEnabled = true;
  glm::vec3 backpackPosition = glm::vec3(0.0f);
  float backpackScale = 0.05f;
  LightManager lightManager;
  ProgramState()
      : camera(glm::vec3(0.0f, 10.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), -89,
               -89) {}
};

ProgramState *programState;

void DrawImGui(ProgramState *programState);

int main() {
  // glfw: initialize and configure
  // ------------------------------
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

  // glfw window creation
  // --------------------
  GLFWwindow *window =
      glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
  if (window == NULL) {
    std::cout << "Failed to create GLFW window" << std::endl;
    glfwTerminate();
    return -1;
  }
  glfwMakeContextCurrent(window);
  glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
  glfwSetCursorPosCallback(window, mouse_callback);
  glfwSetScrollCallback(window, scroll_callback);
  glfwSetKeyCallback(window, key_callback);
  // tell GLFW to capture our mouse
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  // glad: load all OpenGL function pointers
  // ---------------------------------------
  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
    std::cout << "Failed to initialize GLAD" << std::endl;
    return -1;
  }

  // tell stb_image.h to flip loaded texture's on the y-axis (before loading
  // model).
  //    stbi_set_flip_vertically_on_load(true);

  programState = new ProgramState;
  glEnable(GL_DEPTH_TEST);

  // build and compile shaders
  // -------------------------
  Shader clockShader("resources/shaders/multiple_lights.vs",
                     "resources/shaders/multiple_lights.fs");

  Cube::setupStaticMembers();
  Cube pointLightCube("resources/shaders/point_light_source.vs",
                      "resources/shaders/point_light_source.fs");
  Cube pointLightCube2("resources/shaders/point_light_source.vs",
                       "resources/shaders/point_light_source.fs");

  Square::setupStaticMembers();
  Square planeSquare("resources/shaders/plane_shader.vs",
                     "resources/shaders/plane_shader.fs",
                     "resources/textures/parket.jpeg");

  // load models
  // -----------
  Model ourModel("resources/objects/skoljka/common-cockle.obj");
  ourModel.SetShaderTextureNamePrefix("material.");

  // draw in wireframe
  // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  // render loop
  // -----------
  while (!glfwWindowShouldClose(window)) {
    // per-frame time logic
    // --------------------
    float currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    // input
    // -----
    processInput(window);

    // render
    // ------
    glClearColor(programState->clearColor.r, programState->clearColor.g,
                 programState->clearColor.b, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    programState->lightManager.pointLight.position =
        glm::vec3(2.0 * cos(currentFrame), 2.0f, 2.0 * sin(currentFrame));
    programState->lightManager.applyLightsToShader(
        clockShader, programState->camera.Position);

    // view/projection transformations
    glm::mat4 projection =
        glm::perspective(glm::radians(programState->camera.Zoom),
                         (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
    glm::mat4 view = programState->camera.GetViewMatrix();
    clockShader.setMat4("projection", projection);
    clockShader.setMat4("view", view);

    // render the loaded model
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(
        model,
        programState->backpackPosition); // translate it down so it's at the
                                         // center of the scene
    model =
        glm::rotate(model, glm::radians(120.0f), glm::vec3(0.0f, -1.0f, 0.0f));
    model =
        glm::rotate(model, glm::radians(180.0f), glm::vec3(0.0f, 0.0f, -1.0f));
    model = glm::scale(
        model,
        glm::vec3(programState->backpackScale)); // it's a bit too big for our
                                                 // scene, so scale it down
    clockShader.setMat4("model", model);
    ourModel.Draw(clockShader);

    // render pointLightCube
    model = glm::mat4(1.0f);
    model =
        glm::translate(model, programState->lightManager.pointLight.position);
    model = glm::scale(model, glm::vec3(0.2f));
    pointLightCube.setMVPMatrices(model, view, projection);
    pointLightCube.draw();

    // render pointLightCube
    model = glm::mat4(1.0f);
    model =
        glm::translate(model, programState->lightManager.pointLight.position +
                                  glm::vec3(0.5 * cos(3 * currentFrame), 0.0f,
                                            0.5 * sin(3 * currentFrame)));
    model = glm::rotate(model, 10 * currentFrame, glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::scale(model, glm::vec3(0.12f));
    pointLightCube2.setMVPMatrices(model, view, projection);
    pointLightCube2.draw();

    // render planeSquare
    programState->lightManager.applyLightsToShader(
        planeSquare.getShader(), programState->camera.Position);
    model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(0.0f, -2.0f, 0.0f));
    model =
        glm::rotate(model, glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    //		model = glm::scale(model, glm::vec3(22.0f));
    planeSquare.setMVPMatrices(model, view, projection);
    planeSquare.draw();

    // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved
    // etc.)
    // -------------------------------------------------------------------------------
    glfwSwapBuffers(window);
    glfwPollEvents();
  }

  delete programState;
  // glfw: terminate, clearing all previously allocated GLFW resources.
  // ------------------------------------------------------------------
  glfwTerminate();
  return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this
// frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window) {
  if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    glfwSetWindowShouldClose(window, true);

  if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    programState->camera.ProcessKeyboard(FORWARD, deltaTime);
  if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    programState->camera.ProcessKeyboard(BACKWARD, deltaTime);
  if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    programState->camera.ProcessKeyboard(LEFT, deltaTime);
  if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    programState->camera.ProcessKeyboard(RIGHT, deltaTime);
  if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
    programState->camera.resetCamera();
}

// glfw: whenever the window size changed (by OS or user resize) this callback
// function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow *window, int width, int height) {
  // make sure the viewport matches the new window dimensions; note that width
  // and height will be significantly larger than specified on retina displays.
  glViewport(0, 0, width, height);
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouse_callback(GLFWwindow *window, double xpos, double ypos) {
  if (firstMouse) {
    lastX = xpos;
    lastY = ypos;
    firstMouse = false;
  }

  float xoffset = xpos - lastX;
  float yoffset =
      lastY - ypos; // reversed since y-coordinates go from bottom to top

  lastX = xpos;
  lastY = ypos;

  if (programState->CameraMouseMovementUpdateEnabled)
    programState->camera.ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scroll_callback(GLFWwindow *window, double xoffset, double yoffset) {
  programState->camera.ProcessMouseScroll(yoffset);
}

void key_callback(GLFWwindow *window, int key, int scancode, int action,
                  int mods) {
  if (key == GLFW_KEY_F1 && action == GLFW_PRESS) {
    programState->ImGuiEnabled = !programState->ImGuiEnabled;
    if (programState->ImGuiEnabled) {
      programState->CameraMouseMovementUpdateEnabled = false;
      glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    } else {
      glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }
  }
}
