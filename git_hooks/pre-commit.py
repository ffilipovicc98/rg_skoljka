#!/usr/bin/python3

import subprocess
import os

def isccppfile(filename):
  return filename.endswith('.h') or filename.endswith('.c') or filename.endswith('.hpp') or filename.endswith('.cpp')

dir   = os.curdir
style = 'LLVM'

print('Pre commit reformat: ')

for dir, _, files in os.walk(dir):
  for filename in files:
    filepath = dir + '/' + filename
    if isccppfile(filename):
      print('Reformating file: {}'.format(filepath))
      subprocess.run('clang-format -i -style={} {}'.format(style, filepath), shell=True)
